registration_fees <- if(!exists("registration_fees")) readr::read_csv("data/registration_fees.csv")

registration_fees_table <- function() {
registration_fees %>% 
  reactable(
    .,
    pagination = TRUE,
    showPageSizeOptions = TRUE,
    highlight = TRUE,
    defaultSorted = "Year",
    defaultColDef = colDef(headerClass = "header", align = "left"),
    columns = list(
      Year = colDef(
        name = "Year",
        width = 100,
        defaultSortOrder = "desc",
        filterable = TRUE
      ),
      Reg_type = colDef(
        name = "Type",
        defaultSortOrder = "desc",
        filterable = TRUE
      ),
      Participant_type = colDef(
        name = "Level",
        width = 100,
        defaultSortOrder = "desc",
        filterable = TRUE
      ),
      Conf_reg_fee = colDef(
        name = "Registration Fee",
        width = 100,
        defaultSortOrder = "desc",
        filterable = TRUE
      ),
      Tutorial_fee = colDef(
        name = "Tutorial Fee",
        width = 100,
        defaultSortOrder = "desc",
        filterable = TRUE
      ),
      Childcare_fee_2_10 = colDef(
        name = "Children 2-10 yrs",
        width = 100,
        defaultSortOrder = "desc",
        filterable = TRUE
      ),
      Childcare_fee_10_14 = colDef(
        name = "Children 10-14 yrs",
        width = 100,
        defaultSortOrder = "desc",
        filterable = TRUE
      )
    ),
    compact = TRUE,
    bordered = TRUE,
    class = "categories-tbl"
  )
}