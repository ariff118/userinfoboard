selected_countries <- if(!exists("selected_countries")) readr::read_csv("data/participation_country.csv")

country_map_data <- selected_countries %>% select(-c(region, iso3_code))

y2016 <- country_map_data %>% select(country, "2021") %>% na.omit()

world_view <- function() {
long_data_country <- country_map_data %>% select(-c(continent, income)) %>%
  tidyr::pivot_longer(
    !country, names_to = "year", values_to="participants"
  )
#mp = max(long_data$participants, na.rm = TRUE
long_data_country %>%
  na.omit() %>%
  group_by(year) %>%
  e_charts(country, height = 400, timeline = TRUE) %>%
  e_map(participants, roam = TRUE, zoom = 1.4) %>%
  e_toolbox_feature("dataView") %>%
  e_toolbox_feature("saveAsImage") %>%
  e_tooltip(trigger = "item", formatter = e_tooltip_choro_formatter()) %>%
  e_visual_map(min = 1, max = 500, inRange = list(color = c("#9fc4ee", "#00438d"))) %>%
  e_timeline_serie(
    title = list(
      list(text = "useR! 2016, Stanford, CA, USA"),
      list(text = "useR! 2017, Brussels, Belgium"),
      list(text = "useR! 2018, Brisbane, Australia"),
      list(text = "useR! 2019, Toulouse, France"),
      list(text = "useR! 2020, Online"),
      list(text = "useR! 2021, Online")
    )
  ) %>%
  e_group("2charts") %>%
  e_connect_group("2charts") #%>%
  #e_arrange(map1, bar_continent, bar_income, rows = 2, cols = 2, title = "Timeline")
}

# Create title list
#timeSeriesTitle <- lapply(seq_along(byDate$date), function(i) {
#  return(
#    list(
#      text = byDate$date[[i]],
#      subtext = paste0('各都道府県合計新規', newByDate[[i]], '人')
#    )
#  )
#})

# Add to the plot
#e_timeline_serie(
#  title = timeSeriesTitle
#)

continent_view <- function() {
  long_data_continent <- country_map_data %>% select(-c(country, income)) %>%
    tidyr::pivot_longer(
      !continent, names_to = "year", values_to="participants"
    )
  
long_data_continent %>%
    group_by(year) %>%
    na.omit() %>%
    e_charts(continent, height = 350, timeline = TRUE) %>%
    e_timeline_opts(show = FALSE) %>%
    e_bar(participants) %>%
    e_group("2charts") %>%
    #e_tooltip(trigger = "axis") %>%
    e_toolbox_feature("dataView") %>%
    e_toolbox_feature("saveAsImage")
}

income_view <- function() {
  long_data_income <- country_map_data %>% select(-c(country, continent)) %>%
    tidyr::pivot_longer(
      !income, names_to = "year", values_to="participants"
    )
  
long_data_income %>%
    group_by(year) %>%
    na.omit() %>%
    e_charts(income, height = 350, timeline = TRUE) %>%
    e_timeline_opts(show = FALSE) %>%
    e_bar(participants) %>%
    e_group("2charts") %>%
    #e_tooltip(trigger = "axis") %>%
    e_toolbox_feature("dataView") %>%
    e_toolbox_feature("saveAsImage")
}


participant_numbers <- if(!exists("participant_numbers")) readr::read_csv("data/participant_numbers.csv")

participant_numbers_chart <- function() {
participant_numbers %>%
  mutate(year = as.character(year)) %>%
  e_charts(year) %>%
  e_bar(Participants) %>%
  e_animation(duration = 1500) %>%
  e_toolbox_feature("dataView") %>%
  e_toolbox_feature("saveAsImage") %>%
  e_tooltip(trigger = "axis")
}