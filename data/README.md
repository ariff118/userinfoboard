# The Data

The information board automatically collects its data from this directory and any information added to the files contained here will be rendered on the information board once the `index.Rmd` file is rendered into a HTML file.

The table below describes the files within this data directory and the years for which each dataset covers.

| Data File | Description | Summary |
| ---	    | ---	  | ---	    |
| conferences.csv | useR! conference location data | 2004 - 2021 |
| key_dates.csv | information about key dates | 2021,2019,2017,2016 |
| keynotes.csv | useR! keynotes | 2004 - 2021 |
| organizing_committee_members.csv | organizing committee member information | 2016 - 2021 |
| modified_gender.csv | gender information of participants | 2016 - 2020 |
| participant_numbers.csv | count of participants | 2016 - 2021 |
| participation_country.csv | countries of participants | 2016 - 2021 |
| program.csv | useR! Program data | 2016 - 2021 |
| program_committee_members.csv | program committee member information | 2004 - 2021 |
| registration_types.csv | count of participants by profession | 2016 - 2021 |
| registration_fees.csv | registration fees | 2016-2019, 2021 |
| sponsors.csv | data on useR! sponsors | 2014 - 2021 |
| tutorials.csv | useR! tutorials | 2017 - 2021 |


# License

The data is provided under a [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) license